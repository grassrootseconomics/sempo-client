import logging

logg = logging.getLogger(__file__)

class BaseClient:
    """Base class for Sempo API client implementations.

    Contains abstract methods for HTTP method interfaces.

    :param host: Sempo API host
    :type host: str
    :param port: Sempo API port
    :type port: int
    :param use_ssl: Whether to use SSL, default True
    :type use_ssl: boolean
    """
    def __init__(self, host='localhost', port=9000, use_ssl=True):
        logg.debug('setting up api client {} {} ssl: {}'.format(host, port, use_ssl))
        self.host = host
        self.port = port
        self.proto = 'http'
        if use_ssl:
            self.proto += 's'
        self.api_url_core = self.proto + '://' + self.host + ':' + str(self.port) + '/api/v{}'
        self.username = None
        self.password = None



    def api_url(self, version=1):
        """Get api url prefix for a given version.

        :param version: API version to generate prefix for
        :type version: int
        :return: Url prefix
        :rtype: str
        """
        return self.api_url_core.format(version)


    @classmethod
    def authorize(self, username, password):
        """Abstract method for authorization

        :raises: NotImplementedError
        """
        raise NotImplementedError()
        pass


    @classmethod
    def get(self, endpoint, version):
        """Abstract method for HTTP GET API calls.

        :param endpoint: API endpoint
        :type endpoint: str
        :param version: API version for prefix generation
        :type endpoint: int
        :raises: NotImplementedError
        """
        raise NotImplementedError()
        pass


    @classmethod
    def post(self, endpoint, data, version):
        """Abstract method for HTTP POST API calls.

        :param endpoint: API endpoint
        :type endpoint: str
        :param data: Data payload, json
        :type data: dict
        :param version: API version for prefix generation
        :type endpoint: int
        :raises: NotImplementedError
        """
        raise NotImplementedError()
        pass


    @classmethod
    def put(self, endpoint, data, version):
        """Abstract method for HTTP POST PUT calls.

        :raises: NotImplementedError
        """
        pass
