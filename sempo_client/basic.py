# standard imports
from urllib import request
from urllib.parse import quote as urlquote
import urllib
import logging
import json
import base64

# local imports
from .error import ClientAuthError
from .client import BaseClient

logg = logging.getLogger(__name__)


# THANKS to https://stackoverflow.com/questions/2407126/python-urllib2-basic-auth-problem
class PreemptiveBasicAuthHandler(urllib.request.HTTPBasicAuthHandler):
    """Handler for basic auth urllib callback.

    :param req: Request payload
    :type req: str
    :return: Request payload
    :rtype: str
    """
    def http_request(self, req):
        url = req.get_full_url()
        realm = None
        user, pw = self.passwd.find_user_password(realm, url)

        if pw:
            raw = "%s:%s" % (user, pw)
            raw_bytes = raw.encode('utf-8')
            auth_base_bytes = base64.encodebytes(raw_bytes)
            auth_base = auth_base_bytes.decode('utf-8')
            auth_base_clean = auth_base.replace('\n', '').strip()
            auth = 'Basic %s' % auth_base_clean
            req.add_unredirected_header(self.auth_header, auth)
            logg.debug('haed {}'.format(req.header_items()))

        return req

    https_request = http_request



class BasicApiClient(BaseClient):
    """Sempo API client with Basic authentication.

    Used for system internal calls.
    
    .. todo:: Add ClientAuthError on authentication error

    :param host: Sempo API host
    :type host: str
    :param port: Sempo API port
    :type port: int
    :param use_ssl: Whether to use SSL, default True
    :type use_ssl: boolean
    """
    def __init__(self, host, port, use_ssl):
        super(BasicApiClient, self).__init__(host, port, use_ssl)
        self.auth = PreemptiveBasicAuthHandler()


    def authorize(self, username, password):
        """Pre-flight provide username and password used for Basic authentication.

        :param username: Login username
        :type username: str
        :param password: Login password
        :type password: str
        """
        self.username = username
        self.password = password


    def get(self, endpoint, version=1):
        """HTTP GET API call.

        :param endpoint: API endpoint
        :type endpoint: str
        :param version: API version for prefix generation
        :type endpoint: int
        :return: result payload, json
        :rtype: dict
        """
        url = self.api_url(version) + endpoint # + '?username=' + urlquote(self.username) + "&password=" + urlquote(self.password)
        req = urllib.request.Request(url)
        req.add_header('Accept', 'application/json')

        self.auth.add_password(
                realm=None,
                uri=url,
                user=self.username,
                passwd=self.password,
                )
        ho = urllib.request.build_opener(self.auth)
        urllib.request.install_opener(ho)

        logg.debug('sending get {}'.format(url))
        try:
            response = urllib.request.urlopen(req)
        except urllib.error.HTTPError as e:
            if e.code == 500:
                logg.error('get error: {} {} {}'.format(url, e.code, e.msg))
            else:
                logg.error('get error: {} {} {} {}'.format(url, e.code, e.msg, e.read().decode('utf-8')))
            raise(e)
        response_json = json.loads(response.read())
        logg.debug('response {}'.format(response_json))
        return response_json



    # TODO: data must be dict, check that it is
    def put(self, endpoint, data, version=1):
        """HTTP PUT API call.

        :param endpoint: API endpoint
        :type endpoint: str
        :param data: Data payload, json
        :type data: dict
        :param version: API version for prefix generation
        :type endpoint: int
        :return: result payload, json
        :rtype: dict
        """
        url = self.api_url(version) + endpoint # + '?username=' + urlquote(self.username) + "&password=" + urlquote(self.password)
        
        req = urllib.request.Request(url, method='PUT')
        req.add_header('Content-Type', 'application/json')
        req.add_header('Accept', 'application/json')

        self.auth.add_password(
                realm=None,
                uri=url,
                user=self.username,
                passwd=self.password,
                )
        ho = urllib.request.build_opener(self.auth)
        urllib.request.install_opener(ho)

        data_str = json.dumps(data)
        data_bytes = data_str.encode('utf-8')
        req.data = data_bytes

        logg.debug('sending put {} {}'.format(url, data_str))
        response = urllib.request.urlopen(req)
        response_json = json.loads(response.read())
        logg.debug('response {}'.format(response_json))
        return response_json



    # TODO: data must be dict, check that it is
    def post(self, endpoint, data, version=1):
        """HTTP POST API call.

        :param endpoint: API endpoint
        :type endpoint: str
        :param data: Data payload, json
        :type data: dict
        :param version: API version for prefix generation
        :type endpoint: int
        :return: result payload, json
        :rtype: dict
        """
        url = self.api_url(version) + endpoint # + '?username=' + urlquote(self.username) + "&password=" + urlquote(self.password)
      
        opener = urllib.request.build_opener(self.auth)
        urllib.request.install_opener(opener)

        req = urllib.request.Request(url, method='POST')
        req.add_header('Content-Type', 'application/json')
        req.add_header('Accept', 'application/json')

        self.auth.add_password(
                realm=None,
                uri=url,
                user=self.username,
                passwd=self.password,
                )
        ho = urllib.request.build_opener(self.auth)
        urllib.request.install_opener(ho)
        
        data_str = json.dumps(data)
        data_bytes = data_str.encode('utf-8')
        req.data = data_bytes

        logg.debug('sending post {} {} {}'.format(url, data_str, req.headers))
        try:
            response = urllib.request.urlopen(req)
        except urllib.error.HTTPError as e:
            logg.error('get error: {} {} {}'.format(e.code, e.headers, e.read()))
            raise(e)
        response_json = json.loads(response.read())
        logg.debug('response {}'.format(response_json))
        return response_json

