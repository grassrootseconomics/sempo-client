class ClientAuthError(Exception):
    """Raised on authentication error from Sempo API

    :param message: Error message
    :type message: str
    """
    def __init__(self, message):
        super().__init__(message)
