"""Mocker for tests using code requiring Sempo API calls

.. moduleauthor:: Louis Holbrook <dev@holbrook.no>
"""
from .api_client import ApiClient
