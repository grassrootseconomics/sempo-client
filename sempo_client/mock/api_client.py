# standard imports
import logging

logg = logging.getLogger(__file__)


class ApiClient:
    """Sempo API mocker.

    New object ids are generated sequentially.

    Currently patches:

    - Version 1:
      * POST /organisation/ 
      * POST /token/
    """
    ids = {
        'organisation': 0,
        'token': 0,
            }

    @staticmethod
    def get(*args, **kwargs):
        """HTTP GET API call

        :returns: json response
        :rtype: dict
        """
        logg.info('mock api get {}'.format(args))
        return {}


    @staticmethod
    def post(*args, **kwargs):
        """HTTP POST API call

        :returns: json response
        :rtype: dict
        """
        logg.info('mock api post {}'.format(args))
        if args[0] == '/token/':
            o = {
                "data": {
                    "token": {
                        "id": ApiClient.ids['token']
                        }
                    }
                    }
            ApiClient.ids['token'] += 1
            return o
        elif args[0] == '/organisation/':
            o = {
                "data": {
                    "organisation": {
                        "id": ApiClient.ids['organisation']
                        }
                    }
                    }
            ApiClient.ids['organisation'] += 1
            return o
            
    @staticmethod
    def put(*args, **kwargs):
        """HTTP PUT API call.

        :returns: json response
        :rtype: dict
        """
        logg.info('mock api put {}'.format(args))
        return "HTTP/1.1 200"

    
    @staticmethod
    def authorize(*args, **kwargs):
        """Noop authorization mock.
        """
        logg.debug('authorize called')
        pass
