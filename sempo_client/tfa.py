# standard imports
from urllib import request
import urllib
import logging
import json

# third-party imports
import pyotp

# local imports
from .error import ClientAuthError
from .client import BaseClient

logg = logging.getLogger(__name__)

OTP_EXPIRY_INTERVAL = 1


class ApiClient(BaseClient):
    """API client implementation with application authentication and optional TOTP TFA

    :param host: Sempo API host
    :type host: str
    :param port: Sempo API port
    :type port: int
    :param use_ssl: Whether to use SSL, default True
    :type use_ssl: boolean
    :param otp_secret: TOTP secret for TFA authentication
    :type otp_secret: str, optional
    """
    def __init__(self, host='localhost', port=9000, use_ssl=True, otp_secret=None):
        super(ApiClient, self).__init__(host, port, use_ssl)
        self.user_id = -1
        self.tfa_token = None
        self.otp = otp_secret
        self.auth_token = None



    def token(self):
        """Returns the currently active authentication token

        :return: Token
        :rtype: str
        """
        tkn = self.auth_token
        if self.tfa_token != None:
            tkn += '|' + self.tfa_token
        return tkn



    def tfa(self):
        """Perform TFA authentication.

        If successful, updates the authorization state.

        :raises HTTPError: Authentication fails or connection error
        """
        totp = pyotp.TOTP(self.otp).now()

        url = self.api_url() + '/auth/tfa/'
        req = urllib.request.Request(url)
        data = {
            'otp': totp,
            'otp_expiry_interval': OTP_EXPIRY_INTERVAL,
            }
        data_str = json.dumps(data)
        data_bytes = data_str.encode('utf-8')
        req.add_header('Content-Type', 'application/json')
        req.add_header('Accept', 'application/json')
        req.add_header('Authorization', self.auth_token)
        req.data = data_bytes

        #logg.debug('sending tfa request {} {}'.format(url, data_str))
        logg.debug('sending tfa request {}'.format(url))
        response = urllib.request.urlopen(req)
        response_json = json.loads(response.read())
        logg.debug('response tfa {}'.format(response_json))
        self.tfa_token = response_json['tfa_auth_token']
        self.user_id = response_json['user_id']
        logg.info('logged in as user id {}'.format(self.user_id))



    def authorize(self, email, password):
        """Perform API authentication.

        If successful, updates the authorization state.

        :param email: Login email
        :type email: str
        :param password: Login password
        :type password: str
        :raises HTTPError: Connection error
        :raises sempo_client.ClientAutherror: Authentication failure
        """
        url = self.api_url() + '/auth/request_api_token/'
        req = urllib.request.Request(url)
        data = {
            'email':    email,
            'password': password,
            }
        #logg.debug('urllib {} {} {}'.format(urllib.__file__, url, data))
        data_str = json.dumps(data)
        data_bytes = data_str.encode('utf-8')
        req.add_header('Content-Type', 'application/json')
        req.add_header('Accept', 'application/json')
        req.data = data_bytes

        #logg.debug('sending api token request {} {}'.format(url, data_str))
        logg.debug('sending api token request {}'.format(url)) #, data_str))
        status = 0
        token = None
        try:
            response = urllib.request.urlopen(req)
            response_json = json.load(response)
            self.auth_token = response_json['auth_token']
        except urllib.error.HTTPError as e:
            if e.code != 401:
                raise(e)

            response_json = json.load(e)
            logg.debug('response authorize {}'.format(response_json))
            tfa_failure = response_json['tfa_failure']
            self.auth_token = response_json['auth_token']
            if tfa_failure:
                if self.otp == None:
                    raise ClientAuthError('TFA needed but opt missing')
                self.tfa()
                
        logg.debug('{}|{}'.format(self.auth_token, self.tfa_token))



    def get(self, endpoint, version=1):
        """HTTP GET API call.

        :param endpoint: API endpoint
        :type endpoint: str
        :param version: API version for prefix generation
        :type endpoint: int
        :return: result payload, json
        :rtype: dict
        """
        url = self.api_url(version) + endpoint
        req = urllib.request.Request(url)
        req.add_header('Accept', 'application/json')
        if self.auth_token != None:
            req.add_header('Authorization', self.token())

        logg.debug('sending get {}'.format(url))
        try:
            response = urllib.request.urlopen(req)
        except urllib.error.HTTPError as e:
            if e.code == 500:
                logg.error('get error: {} {} {}'.format(url, e.code, e.msg))
            else:
                logg.error('get error: {} {} {} {}'.format(url, e.code, e.msg, e.read().decode('utf-8')))
            raise(e)
        response_json = json.loads(response.read())
        logg.debug('response {}'.format(response_json))
        return response_json



    # TODO: data must be dict, check that it is
    def put(self, endpoint, data, version=1):
        """HTTP PUT API call.

        :param endpoint: API endpoint
        :type endpoint: str
        :param data: Data payload, json
        :type data: dict
        :param version: API version for prefix generation
        :type endpoint: int
        :return: result payload, json
        :rtype: dict
        """
        url = self.api_url(version) + endpoint
        
        req = urllib.request.Request(url, method='PUT')
        req.add_header('Content-Type', 'application/json')
        req.add_header('Accept', 'application/json')
        if self.auth_token != None:
            req.add_header('Authorization', self.token())
        data_str = json.dumps(data)
        data_bytes = data_str.encode('utf-8')
        req.data = data_bytes

        logg.debug('sending put {} {}'.format(url, data_str))
        response = urllib.request.urlopen(req)
        response_json = json.loads(response.read())
        logg.debug('response {}'.format(response_json))
        return response_json



    # TODO: data must be dict, check that it is
    def post(self, endpoint, data, version=1):
        """HTTP POST API call.

        :param endpoint: API endpoint
        :type endpoint: str
        :param data: Data payload, json
        :type data: dict
        :param version: API version for prefix generation
        :type endpoint: int
        :return: result payload, json
        :rtype: dict
        """
        url = self.api_url(version) + endpoint
        
        req = urllib.request.Request(url, method='POST')
        req.add_header('Content-Type', 'application/json')
        req.add_header('Accept', 'application/json')
        if self.auth_token != None:
            req.add_header('Authorization', self.token())
        data_str = json.dumps(data)
        data_bytes = data_str.encode('utf-8')
        req.data = data_bytes

        logg.debug('sending post {} {}'.format(url, data_str))
        try:
            response = urllib.request.urlopen(req)
        except urllib.error.HTTPError as e:
            logg.error('get error: {} {}'.format(e.code, e.read()))
            raise(e)
        response_json = json.loads(response.read())
        logg.debug('response {}'.format(response_json))
        return response_json

