from setuptools import setup

setup(
        name='sempo-client',
        version='0.2.1',
        description='API library for the sempo platform',
        author='Louis Holbrook',
        author_email='dev@holbrook.no',
        packages=[
            'sempo_client',
            'sempo_client.extensions',
            'sempo_client.mock',
            ],
        install_requires=[
            'pyotp==2.4.0',
            'requests==2.24.0',
            ],
        url='https://gitlab.com/grassrootseconomics/sempo-client',
        license='GPL3',
        classifiers=[
            'Programming Language :: Python :: 3',
            'Operating System :: OS Independent',
            'Development Status :: 4 - Beta',
            'Environment :: Other Environment',
            'Intended Audience :: Developers',
            'License :: OSI Approved :: GNU General Public License v3 or later (GPLv3+)',
            'Topic :: Software Development :: Libraries :: Python Modules',
            ],
        )
