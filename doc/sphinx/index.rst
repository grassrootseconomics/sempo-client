.. sempo-client documentation master file, created by
   sphinx-quickstart on Sun Sep 20 17:20:35 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to sempo-client's documentation!
========================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

Module sempo_client
===================

.. automodule:: sempo_client
   :members:

base client
***********

.. automodule:: sempo_client.client
   :members:

client for basic authentication
*******************************

.. automodule:: sempo_client.basic
   :members:

client for api/tfa authentication
*********************************

.. automodule:: sempo_client.tfa
   :members:

mock client for testing
***********************

.. automodule:: sempo_client.mock

.. automodule:: sempo_client.mock.api_client
   :members:


Extensions
==========

.. automodule:: sempo_client.extensions.osm
   :members:



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
